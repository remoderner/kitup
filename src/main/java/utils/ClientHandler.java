/*
package utils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger log = LogManager.getLogger(ClientHandler.class);
    private final ByteBuf request;

    public ClientHandler(String projectName, String componentName, String userName) {
        String data = projectName + "." + componentName + "." + userName;
        request = Unpooled.wrappedBuffer(data.getBytes(CharsetUtil.UTF_8));
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        log.debug("Channel is active: " + request);
        ctx.writeAndFlush(request);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) {
        log.debug("Channel is read: " + obj);
        //ctx.write(obj);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        //ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("error in client", cause);
        ctx.close();
    }
}
*/
package classroom;

public class Server {
    private String serverName;
    private String serviceNameQortes;
    private String serviceNameQortesDB;
    private String exeNameQortes;
    private String exeNameQortesDB;
    private String lastVersionDirName;
    private String serverDirName;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServiceNameQortes() {
        return serviceNameQortes;
    }

    public void setServiceNameQortes(String serviceNameQortes) {
        this.serviceNameQortes = serviceNameQortes;
    }

    public String getServiceNameQortesDB() {
        return serviceNameQortesDB;
    }

    public void setServiceNameQortesDB(String serviceNameQortesDB) {
        this.serviceNameQortesDB = serviceNameQortesDB;
    }

    public String getExeNameQortes() {
        return exeNameQortes;
    }

    public void setExeNameQortes(String exeNameQortes) {
        this.exeNameQortes = exeNameQortes;
    }

    public String getExeNameQortesDB() {
        return exeNameQortesDB;
    }

    public void setExeNameQortesDB(String exeNameQortesDB) {
        this.exeNameQortesDB = exeNameQortesDB;
    }

    public String getLastVersionDirName() {
        return lastVersionDirName;
    }

    public void setLastVersionDirName(String lastVersionDirName) {
        this.lastVersionDirName = lastVersionDirName;
    }

    public String getServerDirName() {
        return serverDirName;
    }

    public void setServerDirName(String serverDirName) {
        this.serverDirName = serverDirName;
    }
}
